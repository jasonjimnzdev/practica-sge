# README #

Práctica 01 Sistemas de Gestión Empresarial

### https://bitbucket.org/jasonjimnzdev/practica-sge ###

### Clonar repositorio: git git@bitbucket.org:jasonjimnzdev/practica-sge.git ###

### Pedir invicatión por email a: jasonjimenezcruz@gmail.com ###


### Descripción ###

* Desarrollar utilizando Flask y SQLAlchemy un portal dinámico
* El HTML no tiene que estar necesariamente bien hecho
* La base de datos puede estar en SQLite
* Se pueden utilizar librerías y/o frameworks
* El proyecto cuenta para la nota final
* Cuanto más modulado esté mejor

### Instalación ###

* Crear carpeta con tu nombre y apellidos en la carpeta proyectos
* Dentro de la carpeta crear un entorno virtual llamado "env" y utilizar ese virtualenv
* Junto a la carpeta "env", crear una carpeta con el proyecto
* Dentro de la carpeta del proyecto hacer desde consola "pip freeze > requirements.txt" para tener los requisitos del entorno virtual
* Junto a la carpeta "env" y la del proyecto crear un fichero .gitignore con lo siguiente:
* *.pyc
* /env/

* Crear una rama con la siguiente estructura nombre_apellido1_apellido2
** git checkout -b nombre_apellido1_apellido2
* Subir los cambios: git push origin nombre_apellido1_apellido2 

### Extra ###

* Se valorará positivamente el uso de excepciones y tratamiento de errores
* Se valorará positivamente el uso de la librería requests
* Se valorará positivamente el uso de la librería BeautifulSoup

### Ejemplos ###

* En la carpeta ejemplos habrá código listo para entender como funcionan ciertos elementos
* En la carpeta snippets habrá código reutilizable ( copia/pega ), para aprovecharlo en la aplicación
* En la carpeta skeleton estará un esqueleto de aplicación reutilizable para utilizarlo a modo de plantilla