# -*- coding: utf-8 -*-
import json

def createGeoSpatialJson(name, amenity, latitude, longitude, popupContent = ""):
    geoSpatial = {
        "type": "Feature",
        "properties": {
            "name": name,
            "amenity": amenity,
            "popupContent": popupContent
        },
        "geometry": {
            "type": "Point",
            "coordinates": [latitude, longitude]
        }   
    }
    return json.dumps(geoSpatial)